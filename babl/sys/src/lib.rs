#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use libc::{c_char, c_int, c_long, c_uint, c_void};

pub const BABL_MAJOR_VERSION: u32 = 0;
pub const BABL_MINOR_VERSION: u32 = 1;
pub const BABL_MICRO_VERSION: u32 = 73;
pub const BABL_ALPHA_FLOOR: f64 = 0.0000152587890625;
pub const BABL_ALPHA_FLOOR_F: f64 = 0.0000152587890625;

pub const BablSpaceFlags_BABL_SPACE_FLAG_NONE: BablSpaceFlags = 0;
pub const BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE: BablSpaceFlags = 1;

pub type BablSpaceFlags = c_uint;

pub const BablModelFlag_BABL_MODEL_FLAG_ALPHA: BablModelFlag = 2;
pub const BablModelFlag_BABL_MODEL_FLAG_ASSOCIATED: BablModelFlag = 4;
pub const BablModelFlag_BABL_MODEL_FLAG_INVERTED: BablModelFlag = 8;
pub const BablModelFlag_BABL_MODEL_FLAG_LINEAR: BablModelFlag = 1024;
pub const BablModelFlag_BABL_MODEL_FLAG_NONLINEAR: BablModelFlag = 2048;
pub const BablModelFlag_BABL_MODEL_FLAG_PERCEPTUAL: BablModelFlag = 4096;
pub const BablModelFlag_BABL_MODEL_FLAG_GRAY: BablModelFlag = 1048576;
pub const BablModelFlag_BABL_MODEL_FLAG_RGB: BablModelFlag = 2097152;
pub const BablModelFlag_BABL_MODEL_FLAG_CIE: BablModelFlag = 8388608;
pub const BablModelFlag_BABL_MODEL_FLAG_CMYK: BablModelFlag = 16777216;

pub type BablModelFlag = c_uint;

pub const BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL: BablIccIntent = 0;
pub const BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC: BablIccIntent = 1;
pub const BablIccIntent_BABL_ICC_INTENT_SATURATION: BablIccIntent = 2;
pub const BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC: BablIccIntent = 3;
pub const BablIccIntent_BABL_ICC_INTENT_PERFORMANCE: BablIccIntent = 32;
pub type BablIccIntent = c_uint;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct _Babl {
    _unused: [u8; 0],
}
pub type Babl = _Babl;

pub type BablFuncLinear = Option<
    unsafe extern "C" fn(
        conversion: *const Babl,
        src: *const c_char,
        dst: *mut c_char,
        n: c_long,
        user_data: *mut c_void,
    ),
>;

pub type BablFuncPlanar = Option<
    unsafe extern "C" fn(
        conversion: *const Babl,
        src_bands: c_int,
        src: *mut *const c_char,
        src_pitch: *mut c_int,
        dst_bands: c_int,
        dst: *mut *mut c_char,
        dst_pitch: *mut c_int,
        n: c_long,
        user_data: *mut c_void,
    ),
>;

extern "C" {
    //=========================================================================
    // Other functions
    //=========================================================================
    pub fn babl_get_version(major: *mut c_int, minor: *mut c_int, micro: *mut c_int);
    pub fn babl_init();
    pub fn babl_exit();

    pub fn babl_type(name: *const c_char) -> *const Babl;
    pub fn babl_introspect(babl: *const Babl);

    pub fn babl_sampling(horizontal: c_int, vertical: c_int) -> *const Babl;
    pub fn babl_icc_get_key(
        icc_data: *const c_char,
        icc_length: c_int,
        key: *const c_char,
        language: *const c_char,
        country: *const c_char,
    ) -> *mut c_char;
    pub fn babl_fish(
        source_format: *const c_void,
        destination_format: *const c_void,
    ) -> *const Babl;
    pub fn babl_fast_fish(
        source_format: *const c_void,
        destination_format: *const c_void,
        performance: *const c_char,
    ) -> *const Babl;
    pub fn babl_process(
        babl_fish: *const Babl,
        source: *const c_void,
        destination: *mut c_void,
        n: c_long,
    ) -> c_long;
    pub fn babl_process_rows(
        babl_fish: *const Babl,
        source: *const c_void,
        source_stride: c_int,
        dest: *mut c_void,
        dest_stride: c_int,
        n: c_long,
        rows: c_int,
    ) -> c_long;
    pub fn babl_get_name(babl: *const Babl) -> *const c_char;
    pub fn babl_type_new(first_arg: *mut c_void, ...) -> *const Babl;
    pub fn babl_new_palette(
        name: *const c_char,
        format_u8: *mut *const Babl,
        format_u8_with_alpha: *mut *const Babl,
    ) -> *const Babl;
    pub fn babl_new_palette_with_space(
        name: *const c_char,
        space: *const Babl,
        format_u8: *mut *const Babl,
        format_u8_with_alpha: *mut *const Babl,
    ) -> *const Babl;
    pub fn babl_palette_set_palette(
        babl: *const Babl,
        format: *const Babl,
        data: *mut c_void,
        count: c_int,
    );
    pub fn babl_palette_reset(babl: *const Babl);
    pub fn babl_set_user_data(babl: *const Babl, data: *mut c_void);
    pub fn babl_get_user_data(babl: *const Babl) -> *mut c_void;

    //=========================================================================
    // BablConversion
    //=========================================================================
    pub fn babl_conversion_new(first_arg: *const c_void, ...) -> *const Babl;
    pub fn babl_conversion_get_source_space(conversion: *const Babl) -> *const Babl;
    pub fn babl_conversion_get_destination_space(conversion: *const Babl) -> *const Babl;

    //=========================================================================
    // BablComponent
    //=========================================================================
    pub fn babl_component_new(first_arg: *mut c_void, ...) -> *const Babl;
    pub fn babl_component(name: *const c_char) -> *const Babl;

    //=========================================================================
    // BablModel
    //=========================================================================
    pub fn babl_model(name: *const c_char) -> *const Babl;
    pub fn babl_model_with_space(name: *const c_char, space: *const Babl) -> *const Babl;
    pub fn babl_model_new(first_arg: *mut c_void, ...) -> *const Babl;
    pub fn babl_model_is(babl: *const Babl, model_name: *const c_char) -> c_int;
    pub fn babl_get_model_flags(model: *const Babl) -> BablModelFlag;

    //=========================================================================
    // BablFormat
    //=========================================================================
    pub fn babl_format_get_encoding(babl: *const Babl) -> *const c_char;
    pub fn babl_format_is_palette(format: *const Babl) -> c_int;
    pub fn babl_format_new(first_arg: *const c_void, ...) -> *const Babl;
    pub fn babl_format_n(type_: *const Babl, components: c_int) -> *const Babl;
    pub fn babl_format_is_format_n(format: *const Babl) -> c_int;
    pub fn babl_format_get_n_components(format: *const Babl) -> c_int;
    pub fn babl_format_get_type(format: *const Babl, component_index: c_int) -> *const Babl;
    pub fn babl_format_has_alpha(format: *const Babl) -> c_int;
    pub fn babl_format_get_bytes_per_pixel(format: *const Babl) -> c_int;
    pub fn babl_format_get_model(format: *const Babl) -> *const Babl;
    pub fn babl_format(encoding: *const c_char) -> *const Babl;
    pub fn babl_format_with_space(encoding: *const c_char, space: *const Babl) -> *const Babl;
    pub fn babl_format_exists(name: *const c_char) -> c_int;
    pub fn babl_format_get_space(format: *const Babl) -> *const Babl;

    //=========================================================================
    // BablTrc
    //=========================================================================
    pub fn babl_trc_gamma(gamma: f64) -> *const Babl;
    pub fn babl_trc(name: *const c_char) -> *const Babl;

    //=========================================================================
    // BablSpace
    //=========================================================================
    pub fn babl_space(name: *const c_char) -> *const Babl;
    pub fn babl_space_from_icc(
        icc_data: *const c_char,
        icc_length: c_int,
        intent: BablIccIntent,
        error: *mut *const c_char,
    ) -> *const Babl;
    // Deprecated
    pub fn babl_icc_make_space(
        icc_data: *const c_char,
        icc_length: c_int,
        intent: BablIccIntent,
        error: *mut *const c_char,
    ) -> *const Babl;
    pub fn babl_space_from_chromaticities(
        name: *const c_char,
        wx: f64,
        wy: f64,
        rx: f64,
        ry: f64,
        gx: f64,
        gy: f64,
        bx: f64,
        by: f64,
        trc_red: *const Babl,
        trc_green: *const Babl,
        trc_blue: *const Babl,
        flags: BablSpaceFlags,
    ) -> *const Babl;
    pub fn babl_space_with_trc(space: *const Babl, trc: *const Babl) -> *const Babl;
    pub fn babl_space_get(
        space: *const Babl,
        xw: *mut f64,
        yw: *mut f64,
        xr: *mut f64,
        yr: *mut f64,
        xg: *mut f64,
        yg: *mut f64,
        xb: *mut f64,
        yb: *mut f64,
        red_trc: *mut *const Babl,
        green_trc: *mut *const Babl,
        blue_trc: *mut *const Babl,
    );
    pub fn babl_space_get_rgb_luminance(
        space: *const Babl,
        red_luminance: *mut f64,
        green_luminance: *mut f64,
        blue_luminance: *mut f64,
    );
    pub fn babl_space_get_gamma(space: *const Babl) -> f64;
    pub fn babl_space_get_icc(babl: *const Babl, length: *mut c_int) -> *const c_char;
    pub fn babl_space_from_rgbxyz_matrix(
        name: *const c_char,
        wx: f64,
        wy: f64,
        wz: f64,
        rx: f64,
        gx: f64,
        bx: f64,
        ry: f64,
        gy: f64,
        by: f64,
        rz: f64,
        gz: f64,
        bz: f64,
        trc_red: *const Babl,
        trc_green: *const Babl,
        trc_blue: *const Babl,
    ) -> *const Babl;
    pub fn babl_space_is_cmyk(space: *const Babl) -> c_int;
    pub fn babl_space_is_gray(space: *const Babl) -> c_int;
}
