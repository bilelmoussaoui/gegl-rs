use crate::ObjectType;

define_object!(Component);

impl Component {
    #[doc(alias = "babl_component")]
    pub fn from_name(name: &str) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_component(name.as_ptr() as *const std::ffi::c_char))
        }
    }
}
