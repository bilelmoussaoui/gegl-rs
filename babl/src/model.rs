use crate::{ModelFlag, ObjectType, Space};

define_object!(Model);

impl Model {
    #[doc(alias = "babl_model_with_space")]
    pub fn from_name_with_space(name: &str, space: &Space) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_model_with_space(
                name.as_ptr() as *const std::ffi::c_char,
                space.inner(),
            ))
        }
    }

    #[doc(alias = "babl_model")]
    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_model(name.as_ptr() as *const std::ffi::c_char)) }
    }

    #[doc(alias = "get_flags")]
    #[doc(alias = "babl_get_model_flags")]
    pub fn flags(&self) -> ModelFlag {
        unsafe { std::mem::transmute(ffi::babl_get_model_flags(self.0)) }
    }

    /// Returns whether it's the model name or not
    #[doc(alias = "babl_model_is")]
    pub fn model_is(&self, name: &str) -> bool {
        unsafe { ffi::babl_model_is(self.0, name.as_ptr() as *const std::ffi::c_char) == 1 }
    }
}
