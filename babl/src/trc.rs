use crate::ObjectType;

define_object!(Trc);

impl Trc {
    #[doc(alias = "babl_trc")]
    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_trc(name.as_ptr() as *const std::ffi::c_char)) }
    }

    #[doc(alias = "babl_trc_gamma")]
    pub fn from_gamma(gamma: f64) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_trc_gamma(gamma)) }
    }
}
